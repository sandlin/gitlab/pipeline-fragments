# templates




# About
This repo will be used to store pipeline snippets to be included in other .gitlab-ci.yml
[Reference](https://docs.gitlab.com/ee/ci/yaml/includes.html)

# INDEX
[[_TOC_]]

## dind_service.yml

### About
This yaml file will be used for Docker in Docker pipelines. Rather than downloading the `docker` and the `dind` images from the internet every job, this will download them from a repository within GitLab. Meanwhile, by including this yaml, you no longer need to handle the Docker in Docker service management.

---

### Supported Docker Versions
- 19.03.12

---

### TODO
Add other Docker versions, as needed.

---

### SRC
For context, the source of this file is:
```
.dind_service:
  image: registry.gitlab.com/sandlin/dockerfiles/docker:19.03.12
  services:
    - name: registry.gitlab.com/sandlin/dockerfiles/docker-dind:19.03.12-dind
      alias: docker
```

---

### Usage - changes to your `.gitlab-ci.yml` file
1. Remove the standard docker `image` and `services`
   ```
   image: docker:19.03.12

   services:
     - docker:19.03.12-dind
   ```
1. Include the yaml in your `.gitlab-ci.yml` file.
   ```
    include:
      - project: 'sandlin/gitlab/pipeline-fragments'
        ref: 'tags/dind_19.03.12'
        file: 'dind_service.yml'
   ```
1. In your jobs, extend the `.dind_service`
   1. EX:
      ```
        lint:
          services:
            - docker:19.03.12-dind
          stage: lint
          image: docker:19.03.12
          script:
            - docker-compose build
      ```
      Becomes:
      ```
        lint:
          extends: .dind_service
          stage: lint
          script:
            - docker-compose build
      ```

---



